# ICHLL 11

Voici les sources de la présentation du projet [DISCO-LGE](https://www.collexpersee.eu/projet/disco-lge/) pour la 11ème édition de la conférence [ICHLL](https://www.nerthusproject.com/events).

Télécharger la dernière version des [transparents](https://gitlab.huma-num.fr/disco-lge/ichll11-slides/-/jobs/artifacts/main/raw/transparents.pdf?job=makePDF) (si le lien semble cassé, vérifier [ici](https://gitlab.huma-num.fr/disco-lge/ichll11-slides/-/pipelines) qu'il y a bien une occurrence réussie de la chaîne d'assemblage et dans le cas contraire en lancer une).
