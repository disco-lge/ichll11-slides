---
title: The specificities of encoding encyclopedias\: towards a new standard ?
author: Alice BRENON (ENS Lyon, ICAR, alice.brenon@ens-lyon.fr)
        Denis VIGIER (Université Lyon 2, ICAR, denis.vigier@ens-lyon.fr)
date: 2021 June 16 - 18
institute:
	- \includegraphics[height=30px]{images/LABEX_ASLAN.png} \includegraphics[height=30px]{images/CollEx-Persee.png}
header-includes:
	\usepackage{hyperref}
	\hypersetup{
		colorlinks,
		linkcolor = gray,
		urlcolor = purple
	}
	\addtobeamertemplate{footline}{\hfill\insertframenumber/\inserttotalframenumber}{}
theme: Montpellier

---

# I. Encyclopedias and Dictionaries

## Historical context

> L'**Encyclopédie** ou **Dictionnaire** raisonné des sciences, des arts et des métiers…

A (not so clear) distinction between

::: columns
:::: column
### Dictionaries

- a collection of *signs*
- linguistic aspects (PoS, spelling, pronunciation, etymology…)
- describe the *language*
::::
:::: column
### Encyclopedias

- contextualized knowledge: history, politics, science…
- describe the *world*
- a «modern» thing (*Enlightenment*, 18\textsuperscript{th} cent.)
::::
:::

## Project Collex Persée DISCO-LGE

\center{\includegraphics[width=200pt]{images/lge.jpg}}

- target: «La Grande Encyclopédie» (19\textsuperscript{th} cent.)
- (re)digitize, OCRize, encode and annotate

## The XML-TEI Consortium

- International consortium aiming to produce normalized XML encodings
- Active community (mailing list, meetups…)
- Produces modular *schemas* for many domains
- A lot of [online resources and tools](https://tei-c.org)

\center{\includegraphics[width=150pt]{images/tei.jpg}}

## The *Dictionaries* module

::: columns
:::: column
### Elements

- `entry`
  + `form`
    - `orth`
    - `pron`
    - `gram`
  + `def`
  + `sense`
- `superEntry`
- `entryFree`

::::
:::: column
### Attributes

- `@type` (`xref`, `abbr`, `hom`…)
- `@expand`
- `@norm`
- `@orig`
::::
:::

## Example

![LGE, tome 9 p. 25](images/T9p46.png)

---

![Article «Canéficier», 9th tome](code/canéficier_dictionaries.pdf)

## Difficulties

::: columns

:::: column
![Extract from article «Action», 1st tome](images/action_extract.png)

> «In any case, every time a personal property is claimed, are we not bringing an action that is real and at the same time on a movable ?»
::::

:::: column

### Article «Action»

- 14 pages and a half
- Meta-discursive elements (*«impossible to list»*, *«it seems»*)
- Questions (!)

::::

:::

---

## More generally

### the *dictionaries* module lacks tools for structure

`div` and `p` unavailable under `entry` (even indirectly) \

### «Definitions»

- Named entities (what is the «definition» of a person ? a place ?)
- Short essays (c.f. *Action*)
- Historical perspective (defining what no longer applies)

###

**$\rightarrow$ Encyclopedia don't just define, they collect relevant facts**

# II. Choices

## Customize the TEI ?

::: columns
:::: column
- [ROMA](https://roma.tei-c.org/) makes it easy
- mainly let `div` under `sense`
- $\rightarrow$ brings `p` back
::::
:::: column
**but** schema needed\
\hspace{2em}«sold separately»
::::
:::

![Article «Canéficier» with customized schema](code/canéficier_custom_tei.pdf)

## Stay within the *Core* module ?

- Why use `entry` at all ? Nested `div` are enough
- `head` can be used to encode the `form`
- **less expressive (everything's a `div`, no `orth`, `colloc`)**

![Article «Canéficier» with core elements only](code/canéficier_core.pdf)

## Discussion on the mailing list

Summary of the [discussion](https://listserv.brown.edu/cgi-bin/wa?A2=ind1910&L=TEI-L&D=0&P=15688):

- [FAIR](https://www.go-fair.org/go-fair-initiative/) science (**F**indable, **A**ccessible, **I**nteroperable, **R**eusable) : need for a common working base
- but need for paragraphs: use `seg` ? `ab` ?
- semantic of `def`: what about encyclopedic developments ? use nested `note` ?
- what is less accessible ? using a custom schema or twisting semantics a bit ?

\begin{center}
\textbf{$\rightarrow$ no clear consensus emerged}
\end{center}

# III. Implementation

## Encoding

### [Basnum](https://anr.fr/Projet-ANR-18-CE38-0003)

- *Dictionnaire Universel*, by Antoine Furetière
- project by the [Litt & Arts](https://litt-arts.univ-grenoble-alpes.fr/) and [LATTICE](https://www.lattice.cnrs.fr/) laboratories
- *dictionaries* TEI, `note` for developments

### [Nenufar](http://nenufar.huma-num.fr/presentation/)

- *Petit Larousse illustré*, by Pierre Larousse
- project by the [PRAXILING](https://praxiling.hypotheses.org/) laboratory
- a proper dictionary, *dictionaries* TEI

\vspace{1cm}
\begin{center}
\textbf{$\rightarrow$ we chose to remain in \textit{core}}
\end{center}

## Initial plan

### [GROBID](https://grobid.readthedocs.io/en/latest/Introduction/)

- extract information from PDFs
- encode into XML-TEI
- machine-learning library (requires training)
- has several modules, including one for dictionaries !

### Plan

Train a model for GROBID-dictionaries to segment articles from PDFs and encode them.

---

## Troubles

::: columns
:::: column
- noise (false positive from OCR)
- errors in blocks 
  + segmentation
  + ordering
- but PDFs are hard to fix : (
- no support for encoding outside the *dictionaries* module
::::
:::: column
![Issues with block layout](images/chaoui_dezoom.png)
::::
:::

---

## Tools

::: columns
:::: column
### Soprano
- works from the ALTO files
- applies filtering from `chaoui`
- fixes layout
- segment articles
- encode them to XML-TEI
::::
:::: column
### Chaoui
- web application
- preview ALTO files
- manually select noisy areas to filter them out in `soprano`
- display ordering to check `soprano`'s output
::::
:::


---

## Processing line

1. Digitization + OCR
2. Articles segmentation + XML-TEI encoding
3. Morpho-syntactic annotation

![From the BnF to TXM](images/landscape.png)

## Output

![Example output from `soprano`](code/canéficier.pdf){height=180pt}

# Conclusion

## Results

**Scientific results**

- Encyclopedias inherently differ from dictionaries
- Community at a crossroads
    + officialize *core* for encyclopedias ?
    + rework *dictionaries* to allow them to fit ?

**Technical realisations**

- ALTO-XML viewer **Chaoui**
- Encyclopedia segmenter **soprano**
- XML-TEI annotated version of La Grande Encyclopédie

## Links

**About**

- Collex Persée [DISCO-LGE project](https://www.collexpersee.eu/projet/disco-lge/) (in french)
- next: Project [GÉODE](https://geode-project.github.io/) (in french too)

**Code at HumaNum's [GitLab instance](https://gitlab.huma-num.fr)**

- these slides [https://gitlab.huma-num.fr/disco-lge/ichll11-slides](https://gitlab.huma-num.fr/disco-lge/ichll11-slides)
- [Encoding scenarii](https://gitlab.huma-num.fr/disco-lge/tei-encoding)
- [Soprano](https://gitlab.huma-num.fr/disco-lge/soprano)'s source code
- [Chaoui](https://gitlab.huma-num.fr/alicebrenon/chaoui)'s source code
- The corpus at HumaNum's [Nakala](https://nakala.fr/10.34847/nkl.74eb1xfd)
