---
header-includes:
	\pagestyle{empty}
	\usepackage[left=0cm,top=0cm,right=0cm,nohead,nofoot]{geometry}
	\geometry{
	paperwidth=15.5cm,
	paperheight=5cm,
	margin=0cm
	}
---

```xml
<entry xml:id="canéficier-0">
	<form>
		<orth>CANÉFICIER</orth>.
	</form>
	<sense n="0">
		<def>
			Nom vulgaire du Cassia fistula L., de<lb/>
			la famille des Légumineuses-Cæsalpiniées <xr>(V. <ref>Casse</ref>)</xr>.
		</def>
	</sense>
</entry>
```
