---
header-includes:
	\pagestyle{empty}
	\usepackage[left=0cm,top=0cm,right=0cm,nohead,nofoot]{geometry}
	\geometry{
	paperwidth=14.5cm,
	paperheight=5.5cm,
	margin=0cm
	}
---

```xml
<entry xml:id="canéficier-0">
	<form>
		<orth>CANÉFICIER</orth>.
	</form>
	<sense n="0">
		<div>
			<p>
				Nom vulgaire du Cassia fistula L., de<lb/>
				la famille des Légumineuses-Cæsalpinées <ref>(V. Casse)</ref>.
			</p>
		</div>
	</sense>
</entry>
```
