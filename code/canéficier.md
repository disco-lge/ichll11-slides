---
header-includes:
	\pagestyle{empty}
	\usepackage[left=0cm,top=0cm,right=0cm,nohead,nofoot]{geometry}
	\geometry{
	paperwidth=16.5cm,
	paperheight=12cm,
	margin=0cm
	}
---

```xml
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title>La Grande Encyclopédie, T9, article CAN</title>
			</titleStmt>
			<publicationStmt>
				<publisher>CollEx-Persée DISCO-LGE</publisher>
				<date when="2021" />
			</publicationStmt>
			<sourceDesc>
				<bibl>
					<title>La Grande Encyclopédie</title>
					<publisher>H. Lamirault</publisher>
					<date from="1885" to="1902" />
				</bibl>
			</sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
			<div xml:id="can-0">
				<lb /><head>CAN</head> É Fl Cl ER. Nom vulgaire du Cassia fistula L., de
				<lb />la famille des Légumineuses-Cæsalpiniées (V. Casse).
			</div>
		</body>
	</text>
</TEI>
```
