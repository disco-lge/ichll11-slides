---
header-includes:
	\pagestyle{empty}
	\usepackage[left=0cm,top=0cm,right=0cm,nohead,nofoot]{geometry}
	\geometry{
	paperwidth=14.5cm,
	paperheight=5cm,
	margin=0cm
	}
---

```xml
<div xml:id="canéficier-0">
	<head>
		<hi rend="bold">CANÉFICIER</hi>.
	</head>
	<div type="sense" n="0">
		<p>
			Nom vulgaire du Cassia fistula L., de<lb/>
			la famille des Légumineuses-Cæsalpiniées <ref>(V. Casse)</ref>.
		</p>
	</div>
</div>
```
