SRC=transparents.md live.md
GRAPH=$(wildcard images/*.gv)
IMAGES=$(wildcard images/*.png) $(GRAPH:%.gv=%.png)
CODE=$(wildcard code/*.md)
HIGHLIGHTED_CODE=$(CODE:%.md=%.pdf)
EXTENSIONS=
TARGET=$(SRC:%.md=%.pdf)

all: $(TARGET)

code/%.pdf: code/%.md
	pandoc $< -o $@

transparents.pdf: transparents.md $(IMAGES) $(HIGHLIGHTED_CODE) $(EXTENSIONS:%=%.tex)
	pandoc -s --toc -t beamer --slide-level=2 $(EXTENSIONS:%=-H %.tex) $< -o $@

live.pdf: live.md $(IMAGES) $(HIGHLIGHTED_CODE) $(EXTENSIONS:%=%.tex)
	pandoc -s -t beamer --slide-level=1 $(EXTENSIONS:%=-H %.tex) $< -o $@

%.png: %.gv
	dot -T png $^ -o $@

mrproper:
	rm -f $(TARGET)
