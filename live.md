---
title: The specificities of encoding encyclopedias\: towards a new standard ?
author: Alice BRENON (ENS Lyon, ICAR, alice.brenon@ens-lyon.fr)
        Denis VIGIER (Université Lyon 2, ICAR, denis.vigier@ens-lyon.fr)
date: 2021 June 17
institute:
	- \includegraphics[height=30px]{images/LABEX_ASLAN.png} \includegraphics[height=30px]{images/CollEx-Persee.png}
header-includes:
	\usepackage{hyperref}
	\hypersetup{
		colorlinks,
		linkcolor = gray,
		urlcolor = purple
	}
	\addtobeamertemplate{footline}{\hfill\insertframenumber/\inserttotalframenumber}{}
theme: Montpellier

---

# Encyclopedias and Dictonaries

- \underline{Encyclopedias} vs. Dictionaries
- digitize, OCRize, encode and annotate: La Grande Encyclopédie (19th cent.)
- XML-TEI, *dictionaries* module
- $\rightarrow$ good but doesn't work all the time

# Choices

::: columns
:::: column
## Custom schema

- `div` under `sense`
- $+$ it's easy
- $+$ keeps all the good *dictionaries* stuff
- $-$ `schema` «sold separately»
::::
:::: column
## Stay within *core*

- `div` are enough
- $+$ no additional work
- $+$ self-contained files
- $-$ less expressive
::::
:::

# Implementation

- ~~train GROBID to segment and encode articles from PDF~~
- (too difficult: noise, OLR, PDF edition)
- developed
    + `soprano`: \
	  ALTO `>>=` fixes `>>=` segmented `>>=` encode to (*core*) XML-TEI
    + `chaoui`: webapp to view / edit ALTO files

$\Rightarrow$ encoded articles, CSV files containing fixes

# Links
